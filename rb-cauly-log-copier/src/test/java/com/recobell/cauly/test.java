package com.recobell.cauly;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;

import org.apache.http.client.ClientProtocolException;
import org.joda.time.DateTime;
import org.junit.Test;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

/**
 * 
 * @author 	jackyun
 * @since	2015. 11. 27.
 */
public class test {

//    @Test
    public void test() throws URISyntaxException, ClientProtocolException, IOException {
        String targetString = "SONYUNARA-TEST/order_2015112700_shardId-000000000000_49556228795597460182892435552621449185374089083176878082_test.json.gz";
        if(targetString.contains("test")) System.out.println("good");
        else System.out.println("fail");
//        URI uri = new URIBuilder("http://ec2-52-69-83-36.ap-northeast-1.compute.amazonaws.com")
//        .setPort(5000)
//        .addParameter("retarget_id", "123")
//        .addParameter("ad_code", "1234")
//        .addParameter("action", "purchase")
//        .addParameter("revenue", "12345")
//        .addParameter("currency", "KRW")
//        .addParameter("adid", "123456")
//        .build();
//        System.out.println(uri.toURL().toString());
//        HttpClient client = HttpClientBuilder.create().build();
//        HttpGet httpGet = new HttpGet(uri.toString());
//        client.execute(httpGet);
//        
        Long a = System.currentTimeMillis();
        
        AmazonS3 s3Client = new AmazonS3Client();
        System.out.println(System.currentTimeMillis()- a);

    }
    
    
    @Test
    public void testTimezone() throws Exception {
        
        String utcDateTimeStr = "2016-01-15T06:29:45.539Z";
        Long utcTimestamp = OffsetDateTime.parse(utcDateTimeStr).getLong(ChronoField.INSTANT_SECONDS) * 1000L;
        Long kstTimestamp = utcTimestamp + 9 * 60 * 60 * 1000;
        
        
        System.out.println(utcTimestamp);
        
        System.out.println(new DateTime(utcTimestamp));
        System.out.println(new DateTime(kstTimestamp));
        
        System.out.println(new DateTime(1453258975000L));
        
        
        
    }

}
