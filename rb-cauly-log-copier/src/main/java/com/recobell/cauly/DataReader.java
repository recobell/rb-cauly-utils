package com.recobell.cauly;

import java.io.IOException;

/**
 * 
 * @author djlee
 * @since Jul 3, 2015
 */
public interface DataReader<T>
    extends AutoCloseable
{
    public boolean hasNext()
        throws IOException;

    public T next()
        throws IOException;
}
