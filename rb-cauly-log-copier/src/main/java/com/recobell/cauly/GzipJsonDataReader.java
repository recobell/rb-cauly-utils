package com.recobell.cauly;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * 
 * @author djlee
 * @since Jul 4, 2015
 */
public class GzipJsonDataReader<T>
    extends JsonDataReader<T>
{
    public GzipJsonDataReader(InputStream is, Class<T> claz)
        throws IOException
    {
        super(new GZIPInputStream(is), claz);
    }

}
