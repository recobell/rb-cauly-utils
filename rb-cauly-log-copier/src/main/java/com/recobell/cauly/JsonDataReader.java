package com.recobell.cauly;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author djlee
 * @since Jul 3, 2015
 */
public class JsonDataReader<T>
    implements DataReader<T>
{
    protected final Scanner scanner;
    protected final Class<T> claz;
    protected final ObjectMapper mapper = new ObjectMapper()
        .setSerializationInclusion(Include.NON_NULL)
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    /**
     * 
     * @author djlee
     * @since Jul 3, 2015
     * @param is
     * @param claz
     * @throws UnsupportedEncodingException
     */
    public JsonDataReader(InputStream is, Class<T> claz)
        throws UnsupportedEncodingException
    {
        super();
        this.scanner = new Scanner(is, "utf-8");
        this.claz = claz;
    }

    @Override
    public void close()
        throws IOException
    {
        if (scanner != null) scanner.close();
    }

    /**
     * 
     * @author djlee
     * @since Jul 3, 2015
     * @return
     * @throws IOException
     */
    @Override
    public boolean hasNext()
        throws IOException
    {
        return scanner != null && scanner.hasNext();
    }

    /**
     * 
     * @author djlee
     * @since Jul 3, 2015
     * @return
     * @throws IOException
     */
    @Override
    public T next()
        throws IOException
    {
        if (scanner != null && scanner.hasNextLine()) {
            String line = scanner.nextLine();
            T t = mapper.readValue(line, claz);
            return t;

        }
        return null;
    }

}
