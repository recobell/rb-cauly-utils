package com.recobell.cauly;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.recobell.model.OrderLog;

public class RbCaulyLogCopierHandler
    implements RequestHandler<S3Event, String>
{

    private HttpClient client = HttpClientBuilder.create().build();

    @Override
    public String handleRequest(S3Event s3event, Context context) {

        LambdaLogger logger = context.getLogger();
        
        S3EventNotificationRecord record = s3event.getRecords().get(0);
        String srcBucket = record.getS3().getBucket().getName();
        // Object key may have spaces or unicode non-ASCII characters.
        String srcKey = record.getS3().getObject().getKey()
            .replace('+', ' ');
        
        //to pass the merged log file, merged log file doesn't have the tag shardId
        if(!srcKey.contains("shardId")) return "Merged Log Pass";
        
        AmazonS3Client s3 = new AmazonS3Client();

        String cuid = srcKey.split("/")[0];
        try (DataReader<OrderLog> dr = new GzipJsonDataReader<>(s3.getObject(srcBucket, srcKey).getObjectContent(), OrderLog.class)) {
            while (dr.hasNext()) {
                OrderLog order = dr.next();
                if (order.getAdCode() != null && order.getAdCode().startsWith("cauly_"))
                {
                    sendLog2Test(order, cuid);
                }
                else{
                    if (order.getAdCode() != null)  System.out.println(String.format("no proper adCode : %s", order.getAdCode()));
                    else System.out.println("no Adcode");
                }
            }
        } catch (Exception e) {
            System.err.println("Fail to parsing Log");
        }

        return "Ok";
    }

    private void sendLog2Test(OrderLog order, String cuid)
        throws URISyntaxException, ClientProtocolException, IOException
    {
        URI testUri = new URIBuilder("http://ec2-52-69-83-36.ap-northeast-1.compute.amazonaws.com")
            .setPort(5000)
            .addParameter("retarget_id", cuid)
            .addParameter("rt_code", order.getAdCode().split("cauly_")[1])
            .addParameter("action", "purchase")
            .addParameter("revenue", Double.toString(order.getOrderPrice()))
            .addParameter("currency", "KRW")
            .addParameter("adid", order.getAdId())
            .addParameter("order_id", order.getOrderId())
            .addParameter("purchase_external_product_id", order.getItemId())
            .build();

        URI caulyApiUri = new URIBuilder("http://api.cauly.co.kr/retarget_track/receive_postback")
            .addParameter("retarget_id", cuid)
            .addParameter("rt_code", order.getAdCode().split("cauly_")[1])
            .addParameter("action", "purchase")
            .addParameter("revenue", Double.toString(order.getOrderPrice()))
            .addParameter("currency", "KRW")
            .addParameter("adid", order.getAdId())
            .addParameter("order_id", order.getOrderId())
            .addParameter("purchase_external_product_id", order.getItemId())
            .addParameter("purchase_time", parseToTimestamp(order.getServerTime()).toString())
            .build();

        System.out.println(testUri.toString());
        System.out.println(caulyApiUri.toString());

        try {
            HttpGet httpGet = new HttpGet(testUri.toString());
            client.execute(httpGet);
        } catch (Exception e) {
            System.err.println("Fail to send to test server");
        }

        try {
            HttpGet httpGet = new HttpGet(caulyApiUri.toString());
            client.execute(httpGet);
        } catch (Exception e) {
            System.err.println("Fail to send to cauly server");
        }

    }
    
    private final Long parseToTimestamp(String utcDateTimeStr) {
        if (utcDateTimeStr == null) return null;
        return OffsetDateTime.parse(utcDateTimeStr).atZoneSameInstant(ZoneId.of("UTC+9")).getLong(ChronoField.INSTANT_SECONDS) * 1000L;
    }
    
}
